# gtk4-cpp-meson-skeleton

### Requirements

-   cpp version >= 17
-   gtk4
-   adwaita
-   meson
-   ninja

### Build and run

```
meson setup build --prefix=/usr
cd build
ninja
./app/gtk4-app
```
