#include "Window.h"

Window::Window() : label{"Hello Adwaita, Cpp and Meson!"} {
  set_title("Gtk4 App");
  set_default_size(640, 480);

  set_child(label);
}

Window::~Window() = default;
