#pragma once
#include <gtkmm.h>

struct Window final : public Gtk::Window {
  Window();
  ~Window() override;

  Gtk::Label label;
};
